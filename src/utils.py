import os
import types
from contextlib import contextmanager
import numpy as np
from tqdm import tqdm
from sklearn.pipeline import make_pipeline
from .cross_validation import cv_score


@contextmanager
def transwrite(file, *args, binary=False, **kwargs):
    filename = f"/tmp/temp_writing{os.getpid()}"
    mode = "wb" if binary else "w"
    fd = open(filename, *args, mode=mode, **kwargs)
    success = True
    exception = None
    try:
        yield fd
    except Exception as ex:
        success = False
        exception = ex
    finally:
        if success:
            os.replace(filename, file)
        else:
            raise exception
            
            
class ExpLoss():
    def __init__(self, exp=0.99):
        self.exp = exp
        self.norm_koef = 1
        self.loss = 0
        self.loss_n = 0
        
    def __call__(self, loss):
        self.loss_n += 1
        self.loss = self.loss * self.exp + loss * (1-self.exp)
        self.norm_koef *= self.exp
        return self.loss / (1 - self.norm_koef)


def search_best_model(df, target_column, get_model_jaccard_score, *models, use_tqdm=False):
    scores = []
    iterator = tqdm(models) if use_tqdm else models
    for model in iterator:
        scores.append( cv_score(df, target_column, get_model_jaccard_score, model) )
    indices = np.argsort(scores)[::-1]
    for index in indices:
        model, score = models[index], scores[index]
        print(f"{model}: {score}")


def compare_phrases(df, target_column, model):
    for _, row in df[[target_column + ' words', 'phrases', 'phrases' + model.input_modifier]].dropna().iterrows():
        target_phrase, phrases, inputs = row[0], row[1], row[2]
        phrase_scores, phrase_indexes = model.find_most_probable(inputs, topk=1)
        index, score = phrase_indexes[0], phrase_scores[0]
        print(score)
        print("Target:")
        print(target_phrase)
        print("Prediction:")
        print(phrases[index])
