from os import path
import pandas as pd
from pathlib import Path
from ..constants import root


default_assessment_file = root / "data" / "Assessment.xlsx"


def parse_assessment(assessment_file):
    assessment_file = Path(assessment_file)

    df = pd.read_excel(assessment_file, 1)
    
    df.drop(columns=df.columns[0], inplace=True)
    
    df.rename(columns={column: df.loc[0, column] for column in df.columns[:5]}, inplace=True)
    df.rename(columns={column: df.loc[1, column] for column in df.columns[5:]}, inplace=True)
    df.rename(columns={'E-mail': 'email'}, inplace=True)
    
    df = df.loc[:, df.columns.notnull()]
    
    df.drop([0,1], inplace=True)
    df.drop(1566, inplace=True)
    df = df.append({}, ignore_index=True)
    df.reset_index(inplace=True, drop=True)
    
    competences = df.columns[5:]

    df.loc[0::2, competences + " score"] = df.loc[0::2, competences].to_numpy()
    for competency in competences:
        df.loc[1::2, competency] = df.loc[1::2, competency].apply(
            lambda x: x if (type(x) == str) else None
        )

    df.loc[0::2, competences + " words"] = df.loc[1::2, competences].to_numpy()
    df.drop(labels=competences, axis=1, inplace=True)
    
    df = df.loc[0::2]
    df.reset_index(inplace=True, drop=True)
    
    df.iloc[1::2, :3] = df.iloc[0::2, :3].to_numpy()

    df = df.loc[::2]
    df.reset_index(inplace=True, drop=True)
    
    return df


def get_assessment():
    return parse_assessment( default_assessment_file )
