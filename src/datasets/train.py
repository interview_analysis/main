from itertools import chain
import numpy as np
import pandas as pd
from ..processing import tokenize, stringify, lemmatize
from ..embedding import muse
from ..metrics import max_jaccard_score, maxscore_jaccard_score, subphrase_jaccard_score
from ..phrase import PhraseExtractor
from .assessment import parse_assessment, default_assessment_file
from .text import parse_text, text_columns, default_text_folder
from ..none import notnone


def __extract_maxscores(df: pd.DataFrame, competence_columns: pd.Index):
    max_scores = {column: [] for column in competence_columns}
    targets = {column: [] for column in competence_columns}
    for user_tokens, (_, competences_phrase) in list(zip( 
        df[[ 'task1_text tokens', 'task2_text tokens', 'task3_text tokens' ]].to_numpy(),
        df[competence_columns + ' tokens'].iterrows(),
    )):
        if (user_tokens == None).any():
            for competence_column in competence_columns:
                max_scores[competence_column].append(None)
                targets[competence_column].append(None)
            continue

        user_tokens_lemmatized = [ lemmatize(tokens) for tokens in user_tokens ]
        for competence_column in competence_columns:
            competence_phrase = competences_phrase[competence_column + ' tokens']
            if competence_phrase == None:
                max_scores[competence_column].append(None)
                targets[competence_column].append(None)
                continue
            competence_phrase = lemmatize(competence_phrase)
            max_score, target = 0, None
            for tokens, tokens_lemmatized in zip(user_tokens, user_tokens_lemmatized):
                cur_max_score, cur_pos = maxscore_jaccard_score(tokens_lemmatized, competence_phrase, dif_range=2)
                if cur_max_score > max_score:
                    max_score = cur_max_score
                    target = tokens[cur_pos[0] : cur_pos[1]]
            target = stringify( target )

            max_scores[competence_column].append(max_score)
            targets[competence_column].append(target)

    return max_scores, targets


def __extract_subtargets(df: pd.DataFrame, competence_columns: pd.Index):
    max_scores = {column: [] for column in competence_columns}
    targets = {column: [] for column in competence_columns}
    for user_tokens, (_, competences_phrase) in list(zip( 
        df[[ 'task1_text tokens', 'task2_text tokens', 'task3_text tokens' ]].to_numpy(),
        df[competence_columns + ' tokens'].iterrows(),
    )):
        if (user_tokens == None).any():
            for competence_column in competence_columns:
                max_scores[competence_column].append(None)
                targets[competence_column].append(None)
            continue

        user_tokens_lemmatized = [ lemmatize(tokens) for tokens in user_tokens ]
        for competence_column in competence_columns:
            competence_phrase = competences_phrase[competence_column + ' tokens']
            if competence_phrase == None:
                max_scores[competence_column].append(None)
                targets[competence_column].append(None)
                continue
            competence_phrase = lemmatize(competence_phrase)
            max_score, max_pos, max_tokens_index = 0, None, None
            target = None
            for tokens_index, (tokens, tokens_lemmatized) in enumerate(zip(user_tokens, user_tokens_lemmatized)):
                cur_max_score, cur_pos = subphrase_jaccard_score(tokens_lemmatized, competence_phrase)
                if cur_max_score > max_score:
                    max_score = cur_max_score
                    max_pos = cur_pos
                    max_tokens_index = tokens_index
            
            if max_pos is None:
                max_scores[competence_column].append(None)
                targets[competence_column].append(None)
                continue
            
            tokens = user_tokens[max_tokens_index]
            max_score = max_score / len(competence_phrase)
            target = tokens[ max_pos[0] : max_pos[1] ]

            max_scores[competence_column].append(max_score)
            targets[competence_column].append(target)

    return max_scores, targets


def __join_text_with_assessment(assessment_df, text_df, columns):
    subset = [col+postfix for col in columns for postfix in [" score", " words"]]
    result_df = pd.merge(text_df, assessment_df, how='left', on="email")
    result_df.dropna(axis='index', how='all', subset=subset, inplace=True)
    result_df = result_df[result_df.columns[:5].tolist() + subset]
    result_df.reset_index(inplace=True, drop=True)
    return result_df


def parse_train(
    assessment_file, text_folder, 
    columns=None, embeddings=None, phrase_extractor=None, add_maxscores=False, add_phrase_jaccard_score=False
):
    assessment_df = parse_assessment(assessment_file)
    text_df = parse_text(text_folder)
    
    assessment_df.iloc[:, 5:] = assessment_df.iloc[:, 5:]

    if columns is None:
        columns = assessment_df.columns[5:]

    df = __join_text_with_assessment(assessment_df, text_df, columns)
    df.rename({column: column + ' words' for column in text_columns}, axis='columns', inplace=True)
    for column in columns:
        df[column + " tokens"] = df[column + " words"].apply(tokenize)
    for column in text_columns:
        df[column + " tokens"] = df[column + " words"].apply(tokenize)

    if add_maxscores:
        max_scores, targets = __extract_subtargets(df, columns)
        for column in columns:
            df[column + ' maxscore'] = max_scores[column]
            df[column + ' maxscore words'] = [ stringify(target) for target in targets[column] ]
            df[column + ' maxscore tokens'] = targets[column]

    if phrase_extractor is not None:
        def texts_to_phrases(row):
            result = list(chain(*[ phrase_extractor.transform(text) or [] for text in row ]))
            if len(result) == 0:
                return None
            return result
        df['phrases'] = df[text_columns + ' tokens'].apply(texts_to_phrases, axis=1)
        if add_phrase_jaccard_score:
            target_phrases = [
                (lemmatize(phrase))
                for phrase in df[column + ' tokens'].dropna().tolist()
            ]
            df['phrases jaccard_score'] = df['phrases'].apply(
                notnone(lambda phrases: list(map(
                    lambda phrase: max_jaccard_score(
                        lemmatize(tokenize(phrase)), target_phrases,
                        do_lemmatization=False
                    )[0],
                    phrases
                )))
            )

    if embeddings is not None:
        for embedding in embeddings:
            for column in columns:
                df[column + ' ' + embedding.__name__] = df[column + " words"].apply(
                   notnone(embedding) 
                )
                if add_maxscores:
                    df[column + ' maxscore ' + embedding.__name__] = df[column + " maxscore words"].apply(
                        notnone(embedding)
                    )

            if phrase_extractor is not None:
                df['phrases' + ' ' + embedding.__name__] = df['phrases'].apply(
                    lambda phrases: embedding(phrases, batched=True) if phrases is not None else None)

    return df


def get_train(columns=['мотивация'], embeddings=[muse], phrase_extractor=None, add_maxscores=False, add_phrase_jaccard_score=False):
    return parse_train(default_assessment_file, default_text_folder, columns=columns, embeddings=embeddings,        
        phrase_extractor=phrase_extractor, add_maxscores=add_maxscores, add_phrase_jaccard_score=add_phrase_jaccard_score
    )
