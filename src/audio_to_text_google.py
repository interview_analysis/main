import os
import argparse
from pathlib import Path
import shutil
from transformer import ToAudioTransformer
from multiprocessing import Pool
import speech_recognition as sr

text_folder = Path("data/text_raw_google")
audio_folder = Path("data/audio")


def init_worker(function):
    function.pid = os.getpid()
    function.temp_text_file_path = f"/tmp/user_text_{function.pid}.txt"
    function.toAudioTransformer = ToAudioTransformer(output_filename=f"{function.pid}.wav")
    function.recognizer = sr.Recognizer()

def work(email):
    user_audio_folder = audio_folder / email
    user_text_folder = text_folder / email
    user_text_folder.mkdir(exist_ok=True)
    for audio_path in user_audio_folder.iterdir():
        text_file_path = user_text_folder / (audio_path.stem + ".txt")
        if text_file_path.exists():
            continue
        wav_audio_path = work.toAudioTransformer.transform(str(audio_path))
        audio_file = sr.AudioFile(wav_audio_path)
        with audio_file as f:
            audio = work.recognizer.record(f)
        text = work.recognizer.recognize_google(audio, language='ru-RU')
        with open(work.temp_text_file_path, "w") as f:
            f.write(text)
        shutil.move(work.temp_text_file_path, text_file_path)
    
    
parser = argparse.ArgumentParser(description="Recognize audio using google speech API")
parser.add_argument("--processes", metavar="-p", type=int, default=1, help="number of working processes")
args = parser.parse_args()


# with Pool(processes=args.processes, initializer=init_worker, initargs=(work,)) as pool:
#     pool.map(work, [user_folder.name for user_folder in audio_folder.iterdir()])

init_worker(work)
work("a.n.gel@mail.ru")