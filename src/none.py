def notnone(func):
    def new_func(arg, *args, **kwargs):
        if arg is None:
            return None
        return func(arg, *args, **kwargs)
    return new_func


def unpack(arr):
    return [ item for item in arr if item is not None ]


def pack(items, arr):
    results = []
    item_index = 0
    for elem in arr:
        if elem is None:
            results.append(None)
        else:
            results.append(items[item_index])
            item_index += 1
    if item_index == len(items):
        raise ValueError("'items and 'arr' values does not match")
    return results


def repacker(func):
    def new_func(arr, *args, **kwargs):
        items = unpack(arr)
        result_items = func(items, *args, **kwargs)
        result_arr = pack(result_items, arr)
        return result_arr
    return new_func
