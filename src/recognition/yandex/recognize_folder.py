import argparse
from pathlib import Path
from typing import Union
from recognize_file import FileRecognizer
from multiprocessing import Pool


def _init_work(bucket_name:str, bucket_folder:Union[str, Path]="/", temp_folder:Union[str, Path]="/tmp/audio_recognition"):
    _work.recognizer = FileRecognizer(bucket_name, bucket_folder=bucket_folder, temp_folder=temp_folder)

def _work(input_file, output_file):
    _work.recognizer.recognize(input_file, output_file)
    print(f"{input_file} recognized")

class FolderRecognizer:
    def __init__(self, bucket_name:str, bucket_folder:Union[str, Path]="/", temp_folder:Union[str, Path]="/tmp/audio_recognition", workers=1):
        self.bucket_name = bucket_name
        self.bucket_folder = bucket_folder
        self.temp_folder = temp_folder
        self.workers = workers
    
    def recognize(self, input_folder: Union[str, Path], output_folder: Union[str, Path], skip_exists=True):
        with Pool(processes=self.workers, initializer=_init_work, initargs=(self.bucket_name, self.bucket_folder, self.temp_folder)) as pool:
            input_folder = Path(input_folder)
            for file in input_folder.rglob("*"):
                if not file.is_file():
                    continue
                output_file = output_folder / file.relative_to(input_folder).with_suffix(".json")
                if skip_exists and output_file.exists():
                    continue
                pool.apply(_work, args=(file, output_file))
            pool.close()
            pool.join()
            
            
def recognize_folder(input_folder: Union[str, Path], output_folder: Union[str, Path], bucket_name:str, bucket_folder:Union[str, Path]="/", temp_folder:Union[str, Path]="/tmp/audio_recognition", workers=1):
    recognizer = FolderRecognizer(bucket_name, bucket_folder=bucket_folder, temp_folder=temp_folder, workers=workers)
    recognizer.recognize(input_folder, output_folder)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_folder")
    parser.add_argument("output_folder")
    parser.add_argument("bucket_name")
    parser.add_argument("--workers", "-w", type=int, default=1)
    args = parser.parse_args()
    recognize_folder(args.input_folder, args.output_folder, args.bucket_name, workers=args.workers)
