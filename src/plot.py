import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib import cm
from sklearn.manifold import TSNE
from sklearn.metrics import silhouette_samples


def plot_distribution(df, targets, target_names, embedding_name, reducer=TSNE(metric='cosine'), alpha=1, size=10):
    full_data = []
    columns = [target + ' ' + embedding_name for target in targets]
    for column in columns:
        series = df[column]
        full_data.extend( series[series.notna()].tolist() )
    full_data = reducer.fit_transform(full_data)

    current_data_index = 0
    color_names = list(colors.TABLEAU_COLORS.keys())
    color_values = list(colors.TABLEAU_COLORS.values())    
    for i, column in enumerate(columns):
        color_index = i % len(color_names)
        data = full_data[current_data_index:current_data_index+df[column].count()]
        current_data_index += df[column].count()
        plt.scatter(data[:,0], data[:, 1], edgecolors=color_values[color_index], alpha = alpha, s=size)
    plt.legend(labels=target_names,
               bbox_to_anchor=(0., 1.07, 1., .102), loc='lower left',
               ncol=2, mode='expand', prop={'size': 13})
    plt.xlabel('dimension 1', fontdict={'size': 14})
    plt.ylabel('dimension 2', fontdict={'size': 14})


def plot_silhouette_score(df, columns, embeddings, draw_columns=None):
    if draw_columns is None:
        draw_columns = columns

    embeddings_data = {}
    embeddings_labels = {}
    embeddings_scores = {}
    for embedding in embeddings:
        full_data = []
        full_labels = []
        for i, column in enumerate(columns):
            series = df[column + ' ' + embedding]
            data = series[series.notna()].tolist()
            labels = [i] * len(data)
            full_data.extend(data)
            full_labels.extend(labels)
        embeddings_data[embedding] = np.array(full_data)
        embeddings_labels[embedding] = np.array(full_labels)
        embeddings_scores[embedding] = silhouette_samples(full_data, full_labels)
    
    n_clusters = len(columns)
    y_lower = 10
    for cluster_index, cluster_column in zip(range(n_clusters), columns):
        if cluster_column not in draw_columns:
            continue
        for embedding_index, embedding in enumerate(embeddings):
            full_data = embeddings_data[embedding]
            full_labels = embeddings_labels[embedding]
            scores = embeddings_scores[embedding]
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            cluster_scores = \
                scores[full_labels == cluster_index]

            cluster_scores.sort()

            cluster_size = cluster_scores.shape[0]
            y_upper = y_lower + cluster_size

            color = cm.nipy_spectral(float(embedding_index) / n_clusters)
            plt.fill_betweenx(np.arange(y_lower, y_upper),
                                0, cluster_scores,
                                facecolor=color, edgecolor=color, alpha=0.7)
            plt.legend(
                labels=[emb.replace('_', ' ') for emb in embeddings],
                loc='upper left')

            # Compute the new y_lower for next plot
            y_lower = y_upper + 25  # 10 for the 0 samples


def visualize_values(values):
    print(np.mean(values))
    plt.hist(values, bins=10)
