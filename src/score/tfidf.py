from collections import Counter

wf = Counter()
tf = dict()
idf = Counter()

for email, text1, text2, text3 in zip(df['email'], df['text1'], df['text2'], df['text3']):
    counter = Counter()
    text = text1 + text2 + text3
    for token in text:
        counter[token] += 1
        wf[token] += 1
    for token in counter:
        idf[token] += 1
    tf[email] = counter

    
tf_idf = dict()
for email, user_tf in tf.items():
    user_tf_idf = Counter()
    for token, value in user_tf.items():
        user_tf_idf[token] = value / idf[token]
    tf_idf[email] = user_tf_idf
    
unique_score = dict()
for email, user_tf in tf.items():
    user_corr = 0
    user_tf_idf = tf_idf[email]
    norm_koef = 0
    for token in wf.keys():
        user_corr += user_tf_idf[token]
    unique_score[email] = user_corr
    
df['tfidf_score'] = unique_score.values()