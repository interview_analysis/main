import numpy as np
import pandas as pd
from tqdm import tqdm
from typing import List, Tuple, Dict
import itertools
from .cross_validation import cv_score
from .models import BaseModel


def get_models_embeddings_report(
    df, target_columns, embedding_names, models, get_model_metric,
    folds_num=5, use_tqdm=False):
    result_models, result_params, result_embs, result_columns, result_scores = [], [], [], [], []
    for Model, def_kwargs, search_kwargs in models:
        search_kwargs_vals = list(itertools.product( *list(search_kwargs.values()) ))
        search_kwargs_list = [ 
            { key: value for key, value in zip(search_kwargs.keys(), kwargs_vals) } 
            for kwargs_vals in search_kwargs_vals
        ]
        for search_kwargs in search_kwargs_list:
            kwargs = dict(**def_kwargs, **search_kwargs)
            for embedding_name in embedding_names:
                for target_column in target_columns:
                    result_models.append(Model)
                    result_params.append(kwargs)
                    result_embs.append(embedding_name)
                    result_columns.append(target_column)

    iterator = list(zip(
        result_models, result_params, result_embs, result_columns))
    if use_tqdm:
        iterator = tqdm(iterator)
    for Model, kwargs, embedding_name, target_column in iterator:
        model = Model(embedding_name=embedding_name, **kwargs)
        score = cv_score(df, target_column, get_model_metric, model,
                            folds_num=folds_num, topk=1)
        result_scores.append(score)

    return pd.DataFrame({
        'model' :    [model.__name__ for model in result_models],
        'params':    result_params,
        'embedding': result_embs,
        'target':    result_columns,
        'score' :    result_scores
    })
