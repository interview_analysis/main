def tokenize(string):
    if string is None:
        return None
    return string.strip().lower().replace(",", "").split(" ")


def stringify(tokens):
    if tokens is None:
        return None
    return " ".join(tokens)


__lemmatizer_morph = None
def lemmatize(tokens):
    global __lemmatizer_morph
    if __lemmatizer_morph is None:
        import pymorphy2
        __lemmatizer_morph = pymorphy2.MorphAnalyzer(lang='ru')
    lemmas = [ __lemmatizer_morph.normal_forms(token)[0] for token in tokens ]
    return lemmas


def flatten(texts):
    if type(texts) == str:
        return texts
    elif hasattr(texts, '__iter__'):
        return " ".join([flatten(text) for text in texts if text is not None])
    else:
        raise ValueError("Argument must be string or iterable of string")
