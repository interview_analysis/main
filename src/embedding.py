import numpy as np
import tensorflow as tf
from navec import Navec
import transformers
from joblib import Memory
import tensorflow_hub as hub
import tensorflow_text
from .constants import cache
from .processing import tokenize

memory = Memory(location=cache.as_posix(), verbose=0)


word2vec_model = None

@memory.cache
def word2vec(phrase, batched=False):
    if phrase is None:
        return None
    
    global word2vec_model
    if word2vec_model is None:
        word2vec_model = Navec.load("navec_hudlit_v1_12B_500K_300d_100q.tar")

    def transform(phrase):
        relevant_words = [ word2vec_model[token] for token in tokenize(phrase) if token in word2vec_model.vocab ]
        if len(relevant_words) == 0:
            return None

        embedding = np.array(relevant_words).mean(0)
        return embedding

    if batched:
        return [ transform(p) for p in phrase ]
    else:
        return transform(phrase)



muse_model_link = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"
muse_model = None

@memory.cache
def muse(phrase, batched=False):
    if phrase is None:
        return None

    global muse_model
    if muse_model is None:
        muse_model = hub.load(muse_model_link)

    phrase = tf.constant(phrase)

    if batched:
        batch_size=1024
        results = []
        for start_index in range(0, len(phrase), batch_size):
            results.append(muse_model(phrase[start_index:start_index+batch_size]).numpy())
        result = np.concatenate(results, axis=0)
        return list(result)
    else:
        return muse_model([phrase]).numpy().squeeze()


musecnn_model_link = "https://tfhub.dev/google/universal-sentence-encoder-multilingual/3"
musecnn_model = None

@memory.cache
def musecnn(phrase, batched=False):
    if phrase is None:
        return None

    global musecnn_model
    if musecnn_model is None:
        musecnn_model = hub.load(musecnn_model_link)

    phrase = tf.constant(phrase)

    if batched:
        batch_size=1024
        results = []
        for start_index in range(0, len(phrase), batch_size):
            results.append(musecnn_model(phrase[start_index:start_index+batch_size]).numpy())
        result = np.concatenate(results, axis=0)
        return list(result)
    else:
        return musecnn_model([phrase]).numpy().squeeze()


rubert_model_name = "DeepPavlov/rubert-base-cased-sentence"
rubert_tokenizer = None
rubert_model = None

@memory.cache
def rubert_pooler(phrase, batched=False):
    global rubert_tokenizer, rubert_model
    
    if rubert_tokenizer is None:
        rubert_tokenizer = transformers.AutoTokenizer.from_pretrained(rubert_model_name)
    if rubert_model is None:
        rubert_model = transformers.AutoModel.from_pretrained(rubert_model_name, return_dict=True)

    def process(phrase):
        if type(phrase) != str:
            return None
        encoded = rubert_tokenizer.encode_plus(phrase, return_tensors='pt')
        embedding = rubert_model(**encoded).pooler_output.squeeze().detach().numpy()
        return embedding

    if batched:
        return [process(words) for words in phrase]
    else:
        return process(phrase)

@memory.cache
def rubert_mean(phrase, batched=False):
    global rubert_tokenizer, rubert_model
    
    if rubert_tokenizer is None:
        rubert_tokenizer = transformers.AutoTokenizer.from_pretrained(rubert_model_name)
    if rubert_model is None:
        rubert_model = transformers.AutoModel.from_pretrained(rubert_model_name, return_dict=True)

    def process(phrase):
        if type(phrase) != str:
            return None
        encoded = rubert_tokenizer.encode_plus(phrase, return_tensors='pt')
        embedding = rubert_model(**encoded).last_hidden_state.mean(axis=1).squeeze().detach().numpy()
        return embedding

    if batched:
        return [process(words) for words in phrase]
    else:
        return process(phrase)


labse_preprocessor_name = "https://tfhub.dev/google/universal-sentence-encoder-cmlm/multilingual-preprocess/2"
labse_model_name = "https://tfhub.dev/google/LaBSE/2"
labse_preprocessor = None
labse_model = None

@memory.cache
def labse_pooler(phrase, batched=False):
    global labse_preprocessor, labse_model
    
    if labse_preprocessor is None:
        labse_preprocessor = hub.KerasLayer(labse_preprocessor_name)
    if labse_model is None:
        labse_model = hub.KerasLayer(labse_model_name)

    phrase = tf.constant(phrase)

    if batched:
        return list(labse_model(labse_preprocessor( phrase ))['default'].numpy())
    else:
        return labse_model(labse_preprocessor( [phrase] ))['default'][0]
