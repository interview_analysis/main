from pathlib import Path
import shutil
from transformer import ToAudioTransformer, AudioToTextTransformer
from sklearn.pipeline import make_pipeline

temp_text_file = "/tmp/user_text.txt"

text_folder = Path("data/text_raw")
audio_folder = Path("data/audio")

transformer = make_pipeline(ToAudioTransformer(), AudioToTextTransformer("model-ru"))

for audio_path in audio_folder.iterdir():
    email = audio_path.name
    print(email)

    text_path = text_folder / email
    text_path.mkdir(exist_ok=True)

    for audio_file in audio_path.iterdir():
        print(audio_file)

        basename = audio_file.stem
        text_file = text_path / (basename + ".txt")

        if text_file.exists():
            continue

        tokens = transformer.transform(str(audio_file))
        with open(temp_text_file, "w") as f:
            f.write(" ".join(tokens))

        shutil.move(temp_text_file, text_file)
