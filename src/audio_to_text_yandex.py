import requests
import time
import json
import argparse
import os


parser = argparse.ArgumentParser()
parser.add_argument("-k", "--key", type=str, help="yandex api key")
parser.add_argument("-n", "--num", type=int, help="number of workers")
args = parser.parse_args()
print(args.key)
print(args.num)
if args.key is None:
    args.key = os.environ.get("YANDEX_CREDENTIALS")
if args.key is None:
    raise ValueError("You must set YANDEX_CREDENTIALS environment variable or provide --key argument")

# Укажите ваш API-ключ и ссылку на аудиофайл в Object Storage.
key = args.key


def work(link):
    filelink = 'https://storage.yandexcloud.net/intan/test.wav?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=I89EjR7hpjWoYGE7xm7A%2F20201129%2Fru-central1%2Fs3%2Faws4_request&X-Amz-Date=20201129T210232Z&X-Amz-Expires=3600&X-Amz-Signature=f89b51e15b6e24abc090915cbf30ccc9fb410af1061a68cdbafd17ee770ade77&X-Amz-SignedHeaders=host'

    POST = "https://transcribe.api.cloud.yandex.net/speech/stt/v2/longRunningRecognize"

    body ={
        "config": {
            "specification": {
                "languageCode": "ru-RU",
                "model": "general",
                "profanityFilter": "false",
                "audioEncoding": "LINEAR16_PCM",
                "sampleRateHertz": "48000",
                "audioChannelCount": "2",
            }
        },
        "audio": {
            "uri": filelink
        }
    }

    # Если вы хотите использовать IAM-токен для аутентификации, замените Api-Key на Bearer.
    header = {'Authorization': 'Api-Key {}'.format(key)}

    # Отправить запрос на распознавание.
    req = requests.post(POST, headers=header, json=body)
    data = req.json()

    id = data['id']

    # Запрашивать на сервере статус операции, пока распознавание не будет завершено.
    while True:
        time.sleep(1)
        GET = "https://operation.api.cloud.yandex.net/operations/{id}"
        req = requests.get(GET.format(id=id), headers=header)
        req = req.json()
        if req['done']: break
        print("Not ready")

    # Показать полный ответ сервера в формате JSON.
    print("Response:")
    print(json.dumps(req, ensure_ascii=False, indent=2))

    # Показать только текст из результатов распознавания.
    print("Text chunks:")
    for chunk in req['response']['chunks']:
        print(chunk['alternatives'][0]['text'])