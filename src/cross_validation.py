import numpy as np
import pandas as pd
from tqdm import tqdm


def cv_score(df, target_column, get_model_metric, model, folds_num=5, topk=1, use_tqdm=False):
    scores = []
    folds_size = len(df) // folds_num
    iterator = range(0, folds_num * folds_size, folds_size)
    if use_tqdm:
        iterator = tqdm(iterator)
    for start_index in iterator:
        end_index = start_index + folds_size
        train_df = pd.concat([ df[ : start_index], df[end_index : ] ])
        test_df = df[start_index : end_index]
        model.fit(train_df, target_column)
        scores.append( get_model_metric(test_df, target_column, model, topk=topk).mean() )
    return np.mean(scores)
