import time
import pandas as pd
from urllib.request import urlretrieve
from pathlib import Path
from moviepy import editor
import shutil
from progressbar import ProgressBar

df = pd.read_pickle("merged_dataset.pickle")
df = df.sort_values("email")

temp_video_path = "/tmp/user_video"
temp_audio_name = "/tmp/user_audio.mp3"

audio_folder = Path("data/audio")

def repeat(func, attempts=5, timeout=5):
    counter = 0
    while True:
        try:
            res = func()
            return res
        except Exception as e:
            counter += 1
            if counter >= attempts:
                raise e
        time.sleep(timeout)

for counter, [_, row] in enumerate(df.iterrows()):
    email = row['email']
    task_links = row['task1_link'], row['task2_link'], row['task3_link']

    print(f"{counter+1}/{len(df.index)}: {email}")
    
    folder = audio_folder / email
    folder.mkdir(exist_ok=True)

    for i in range(3):
        if task_links[i] is not None:
            audio_name = folder / f"task{i+1}.mp3"
            if audio_name.exists():
                continue
            video_ext = task_links[i].split(".")[-1]
            temp_video_name = f"{temp_video_path}.{video_ext}"
            print(task_links[i])
            try:
                urlretrieve(task_links[i], temp_video_name)
                try:
                    clip = editor.VideoFileClip(str(temp_video_name))
                    audio = clip.audio
                    audio.write_audiofile(str(temp_audio_name), fps=16000, verbose=False)
                    shutil.move(temp_audio_name, audio_name)
                except:
                    audio = editor.AudioFileClip(str(temp_video_name))
                    audio.write_audiofile(str(temp_audio_name), fps=16000, verbose=False)
                    shutil.move(temp_audio_name, audio_name)
            except Exception as e:
                print(f"Error in parsing {task_links[i]} for user {email}:")
                print(e)
