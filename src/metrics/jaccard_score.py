import typing
import numpy as np
import pandas as pd
import sklearn
from tqdm import tqdm

from src.processing import tokenize, lemmatize
from src.models import BaseModel


def jaccard_score(target, pred, do_lemmatization=True):
    if do_lemmatization:
        target = lemmatize(target)
        pred = lemmatize(pred)

    set1 = set( target )
    set2 = set( pred )
    return len(set1 & set2) / len(set1 | set2)


def max_jaccard_score(target, preds, do_lemmatization=True) -> typing.Tuple[float, int]:
    if do_lemmatization:
        target = lemmatize(target)
    set1 = set( target )

    max_score = 0
    max_index = 0
    for i, pred in enumerate(preds):
        if do_lemmatization:
            pred = lemmatize(pred)
        set2 = set( pred )
        score = len(set1&set2) / len(set1|set2)
        if score > max_score:
            max_score = score
            max_index = i
    return max_score, max_index


def get_model_jaccard_score(df: pd.DataFrame, target_column: pd.Index, model: BaseModel, topk=1, use_tqdm=False):
    scores = []
    phrases = []

    iterator = list(df[['phrases', 'phrases' + model.input_modifier, target_column + ' words']].dropna().iterrows())
    if use_tqdm:
        iterator = tqdm(iterator)
    for _, row in iterator:
        phrase_list, input_list, target = row[0], row[1], row[2]
        target = tokenize(target)
        phrase_list = [tokenize(phrase) for phrase in phrase_list]
        pred_scores, pred_indexes = model.find_most_probable(input_list, topk=topk)
        pred_phrases = [ phrase_list[index] for index in pred_indexes ]
        score, index = max_jaccard_score( target, pred_phrases )
        scores.append( score )
        phrases.append(( target, pred_phrases[0] ))
    return np.array(scores)


def get_model_cross_jaccard_score(df: pd.DataFrame, target_column: pd.Index, model: BaseModel, use_tqdm=False):
    scores = []

    targets_phrase = df[target_column + ' tokens'].dropna()
    targets_phrase = [ lemmatize(phrase) for phrase in targets_phrase ]
    iterator = list(df[['phrases', 'phrases' + model.input_modifier]].dropna().iterrows())
    if use_tqdm:
        iterator = tqdm(iterator)
    for _, row in iterator:
        phrase_list, input_list = row[0], row[1]
        pred_scores, pred_indexes = model.find_most_probable(input_list, topk=1)
        pred_score, pred_index = pred_scores[0], pred_indexes[0]
        pred_phrase = lemmatize(tokenize( phrase_list[pred_index] ))
        score, _ = max_jaccard_score( pred_phrase, targets_phrase, do_lemmatization=False )
        scores.append( score )
    return np.array(scores)


def maxscore_jaccard_score(tokens, target, dif_range=2):
    max_score = 0
    max_pos = None

    for window_size in range(len(target)-dif_range, len(target)+dif_range):
        if window_size < 1 or window_size > len(tokens):
            continue
        len_dif = len(target) - window_size
        target_words = set(target)

        current_intersection = len([ token for token in tokens[0: window_size] if token in target_words])
        score = current_intersection / (window_size + len(target) - min(current_intersection, window_size))
        if score > max_score:
            max_score = score
            max_pos = (0, window_size-1)

        for start_index in range(0, len(tokens) - window_size, 1):
            end_index = start_index + window_size - 1
            if tokens[start_index] in target_words:
                current_intersection -= 1
            if tokens[end_index] in target_words:
                current_intersection += 1
            score = current_intersection / (window_size + len(target) - min(current_intersection, window_size))
            if score > max_score:
                max_score = score
                max_pos = (start_index, end_index)

    if max_pos != None:
        return max_score, max_pos
    else:
        return 0, None


def subphrase_jaccard_score(tokens, target):

    target_words = set(target)
    token_present = [ token in target_words for token in tokens ]

    max_present = 0
    max_pos = None
    left_pos, right_pos = 0, 0
    cur_present = 0
    
    for right_pos in range(0, len(tokens)):
        if token_present[right_pos]:
            cur_present += 1
            if cur_present > max_present:
                max_present = cur_present
                max_pos = (left_pos, right_pos+1)
        else:
            cur_present -= 1
            if cur_present < 0:
                cur_present = 0
                left_pos = right_pos + 1

    return  max_present, max_pos
