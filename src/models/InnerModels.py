import numpy as np
from .BaseModel import BaseModel


class BaseInnerModel(BaseModel):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def fit(self, df, target_column):
        embeddings = [ embedding for embedding in df[target_column + self.input_modifier].dropna() ]
        if self.use_subtarget:
            embeddings.extend([ embedding for embedding in df[target_column + " maxscore" + self.input_modifier].dropna() ])
        self.embeddings = np.array(embeddings)


class InnerTopModel(BaseInnerModel):
    params = ['topn']

    def __init__(self, topn=5, **kwargs):
        super().__init__(**kwargs)
        self.topn = topn
    
    def predict_proba(self, X):
        X = np.array(X)
        product = X @ self.embeddings.T
        probs = product[:, :self.topn].sum(axis=-1) / self.topn
        return probs


class InnerExpModel(BaseInnerModel):
    params = ['exp']

    def __init__(self, exp=1, **kwargs):
        super().__init__(**kwargs)
        self.exp = exp
    
    def predict_proba(self, X):
        X = np.array(X)
        product = X @ self.embeddings.T
        product[product < 0] = 0
        probs = (product ** self.exp).sum(axis=-1) / len(self.embeddings)
        return probs


class InnerSumModel(BaseInnerModel):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    
    def predict_proba(self, X):
        X = np.array(X)
        product = X @ self.embeddings.T
        return product.sum(axis=-1)
