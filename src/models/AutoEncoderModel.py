import numpy as np
import torch
from torch import nn
import torch.nn.functional as F


class AutoEncoderModel(nn.Module):

    _dist_functions = {
        'mse': F.mse_loss,
        'l1':  F.l1_loss,
    }

    def __init__(self, input_size, layers_size, dropout,
                 optimizer, learning_rate, iterations, weight_decay,
                 loss_f, metric_f):
        super().__init__()
        self.iterations = iterations
        self.loss_f = self._dist_functions[loss_f]
        self.metric_f = self._dist_functions[metric_f]
        if torch.cuda.is_available():
            self.device = 'cuda'
        else:
            self.device = 'cpu'

        self.input_linear = nn.Linear(input_size, layers_size[0])
        nn.init.zeros_(self.input_linear.bias)
        nn.init.xavier_uniform_(self.input_linear.weight)
        hiddens = []
        for in_size, out_size in zip(layers_size[:-1], layers_size[1:]):
            hidden = nn.Linear(in_size, out_size)
            nn.init.zeros_(hidden.bias)
            nn.init.xavier_uniform_(hidden.weight)
            hiddens.append(hidden)
        self.hidden_linears = nn.ModuleList(hiddens)
        self.hidden_norms = nn.ModuleList([ nn.BatchNorm1d(size) for size in layers_size[1:] ])
        self.output_linear = nn.Linear(layers_size[-1], input_size)
        self.act = nn.Tanh()
        self.dropout = nn.Dropout(p=dropout)

        if optimizer == 'sgd':
            Optimizer = torch.optim.SGD
        elif optimizer == 'adam':
            Optimizer = torch.optim.Adam
        self.optimizer = Optimizer(self.parameters(), lr=learning_rate, weight_decay=weight_decay)
        self.to(self.device)

    def forward(self, X):
        X = self.input_linear(X)
        X = self.dropout(X)
        X = self.act(X)
        for linear, norm in zip(self.hidden_linears, self.hidden_norms):
            X = linear(X)
            X = self.dropout(X)
            X = self.act(X)
        X = self.output_linear(X)
        return X

    def fit(self, X, Y):
        X = torch.Tensor(X).to(self.device)
        X = X[Y == 1]
        X = X.to(self.device)

        for _ in range(self.iterations):
            self.optimizer.zero_grad()

            out = self.forward(X)
            loss = self.loss_f(out, X)
            loss.backward()
            self.optimizer.step()

    def predict_proba(self, X):
        X = torch.Tensor(X).to(self.device)
        outputs = self.forward(X)
        scores = 1 / self.metric_f(outputs, X, reduction='none').mean(axis=-1)
        result = scores.cpu().detach().numpy()
        return np.concatenate([result[:, np.newaxis], result[:, np.newaxis]], axis=-1)
