import numpy as np
from src.models.BaseModel import BaseModel


class SklearnModel(BaseModel):
    params = ['model', 'negative_samples_frac', "use_target", "use_subtarget"]
    
    def __init__(self, Model,
        embedding_name=None, use_target=True, use_subtarget=False,
        negative_samples_frac=1, use_supersampling=False,
        jaccard_score_threshold=1,
        **model_kwargs
    ):
        super().__init__(embedding_name=embedding_name, use_target=use_target, use_subtarget=use_subtarget)
        self.model = Model(**model_kwargs)
        self.negative_samples_frac = negative_samples_frac
        self.use_supersampling = use_supersampling
        self.jaccard_score_threshold = jaccard_score_threshold

    def fit(self, df, target_column):

        target_embeddings = []
        if self.use_target:
            target_embeddings.extend( df[target_column + self.input_modifier].dropna().tolist() )
        if self.use_subtarget:
            target_embeddings.extend( df[target_column + " maxscore" + self.input_modifier].dropna().tolist() )
        target_embeddings = np.array( target_embeddings )

        phrase_embeddings = np.array( df['phrases' + self.input_modifier].dropna().sum() )
        if self.jaccard_score_threshold < 1:
            phrase_jaccard_scores = np.array( df['phrases' + ' jaccard_score'].dropna().sum() )
            phrase_embeddings = phrase_embeddings[phrase_jaccard_scores <= self.jaccard_score_threshold]
        if self.negative_samples_frac < 1:
            indices = np.random.choice( phrase_embeddings.shape[0], size=int(self.negative_samples_frac*len(phrase_embeddings)) )
            phrase_embeddings = phrase_embeddings[indices]

        if self.use_supersampling:
            super_sampling_size = np.math.ceil(len(phrase_embeddings) / len(target_embeddings))
            target_embeddings = np.concatenate([target_embeddings] * super_sampling_size)
            
        X = np.concatenate([ phrase_embeddings, target_embeddings ])
        Y = [0] * len(phrase_embeddings) + [1] * len(target_embeddings)
        X = np.array(X)
        Y = np.array(Y)

        self.model.fit(X, Y)

    def predict_proba(self, X):
        X = self.model.predict_proba(X)[:, 1]
        return X
