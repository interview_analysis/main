import numpy as np
import pandas as pd


class BaseModel:

    params = []
    input_modifier = ''

    def __init__(self, embedding_name=None, use_target=True, use_subtarget=False):
        if not use_target and not use_subtarget:
            raise ValueError("No any target specified")
        self.use_target = use_target
        self.use_subtarget = use_subtarget
        if embedding_name is not None:
            self.input_modifier = ' ' + embedding_name

    def fit(self, df: pd.DataFrame, target_column: pd.Index):
        pass

    def predict(self, X):
        return np.argmax(self.predict_proba(X), axis=-1)

    def predict_proba():
        pass

    def find_most_probable(self, inputs, topk=1):
        phrases_scores = self.predict_proba(inputs)
        indices = phrases_scores.argsort(axis=0)[-topk:]
        result_scores = phrases_scores[indices]
        return result_scores, indices

    def __str__(self):
        attributes_string = ', '.join([
            f'{key}={self.__dict__[key]}' for key in self.params
        ])
        return f"{self.__class__.__name__}({attributes_string})"

    def __repr__(self):
        return self.__str__()
