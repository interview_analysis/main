import numpy as np

def LevenshteinDistance(source, target):

    m, n = len(source)+1, len(target)+1

    d = np.zeros(shape=(m, n), dtype=np.int32)
    d[0, :] = np.arange(n)
    starts = np.zeros_like(d)
    starts[:, 0] = np.arange(m)
 
    for j in range(1, n):
        for i in range(1, m):
            if source[i-1] == target[j-1]:
                substitutionCost = 0
            else:
                substitutionCost = 1

            positions = [ (i-1, j), (i, j-1), (i-1, j-1) ]
            costs = [1, 1, substitutionCost]
            pos_number = np.argmin(list(map(lambda x: d[x[0]]+x[1], zip(positions, costs))))
            d[i, j] = d[positions[pos_number]] + costs[pos_number]
            starts[i, j] = starts[positions[pos_number]]
 
    end = np.argmin(d[:,-1])
    start = starts[end, -1]
    print(d)
    print(starts)
    return start, end, d[end, -1]
